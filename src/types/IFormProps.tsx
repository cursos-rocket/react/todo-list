import { ITask } from "./ITask";

export interface FormProps {
    onAdd: (task: ITask) => void
};
