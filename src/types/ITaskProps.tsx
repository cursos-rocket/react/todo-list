import { ITask } from "./ITask";

export interface ITaskProps {
    task: ITask,
    onRemove: (id: number) => void,
    onToggle: (id: number) => void
};