import { ITask } from "./ITask";

export interface IListProps {
    tasks: ITask[],
    onRemoveTask: (id: number) => void,
    onToggleTask: (id: number) => void
};