
import { ChangeEvent, FormEvent, useState } from "react";
import styles from "./Form.module.css";
import { FormProps } from "../../types/IFormProps";
import { ITask } from "../../types/ITask";

export function Form({onAdd}: FormProps) {

    const [newTaskDescription, setNewTaskDescription] = useState<string>("");

    function handleNewTaskChage(e: ChangeEvent<HTMLInputElement>) {
        e.target.setCustomValidity("");
        setNewTaskDescription(e.target.value);
    }

    function handleSubmitNewTask(e: FormEvent) {
        e.preventDefault();
        const newTask: ITask = {
            id: new Date().getTime(),
            description: newTaskDescription,
            isDone: false
        };
        onAdd(newTask);
        setNewTaskDescription("");
    }
    
    return (
        <>
            <form className={styles.formSection} onSubmit={handleSubmitNewTask}>
                <input 
                    type="text"
                    name="newTask"
                    className={styles.input}
                    value={newTaskDescription}
                    placeholder="Adicione uma nova tarefa"
                    onChange={handleNewTaskChage}
                    required
                />

                <button type="submit" className={styles.submit}>
                    Adicionar
                </button>
            </form>
        </>
    );
}