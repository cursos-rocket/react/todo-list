
import { Check, Trash } from "phosphor-react";
import styles from "./Task.module.css";
import { ITaskProps } from "../../types/ITaskProps";

export function Task({task, onRemove, onToggle}: ITaskProps) {
    return (
        <>
            <div className={styles.container}>
                <label htmlFor="checkbox" onClick={() => onToggle(task.id)}>
                    <input type="checkbox" checked={task.isDone} />
                    <span className={styles.checkbox}>
                        {task.isDone && <Check weight="bold" /> }
                    </span>
                    <p>{task.description}</p>
                </label>
                <button className={styles.delete} title="Remover tarefa" onClick={() => onRemove(task.id)}>
                    <Trash size={16} />
                </button>
            </div>
        </>
    );
}