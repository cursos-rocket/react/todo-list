
import { IListProps } from '../../types/IListProps';
import { Empty } from '../Empty/Empty';
import { Task } from '../Task/Task';
import styles from './List.module.css';

export function List({tasks, onRemoveTask, onToggleTask}: IListProps) {

    function count() {
        return tasks.length;
    }
    function countTasksDone() {
        const allDone = tasks.filter(task => {
            return task.isDone;
        });

        return (allDone.length > 0) ? `${allDone.length} de ${count()}` : 0;
    }
    return (
        <>
        <div className={styles.wrapper}>
            <header className={styles.header}>
                <p>
                    <strong>Tarefas criadas</strong>
                    <span className={styles.badge}>{count()}</span>
                </p>
                
                <p>
                    <strong>Concluídas</strong>
                    <span className={styles.badge}>{countTasksDone()}</span>
                </p>
            </header>
            <main>
                {(tasks.length > 0 ? (
                    tasks.map(task => {
                        return (
                            <Task
                                task={task}
                                onToggle={onToggleTask}
                                onRemove={onRemoveTask}
                            />
                        );
                    })
                )
                :
                <Empty />)}

            </main>
        </div>
        
        </>
    )
}