import styles from './App.module.css';
import { Header } from './components/Header/Header';
import "./global.css";
import { Form } from './components/Form/Form';
import { List } from './components/List/List';
import { useState } from 'react';
import { ITask } from './types/ITask';

function App() {

  const [tasks, setTasks] = useState<ITask[]>([]);

  function newTask(task: ITask) {
    setTasks([...tasks, task]);
  }

  function toggleTask(id: number) {
    const allTasks = tasks.map(task => {
      if (task.id === id) {
        task = {...task, isDone: !task.isDone}
      }
      return task;
    });
    setTasks(allTasks);
  }

  function removeTask(id: number) {
    const allTasks = tasks.filter(task => {
      return task.id != id;
    });
    setTasks(allTasks);
  }

  return (
    <>
    <Header />

    <div className={styles.wrapper}>
        <Form onAdd={newTask} />
        <List 
          tasks={tasks}
          onRemoveTask={removeTask}
          onToggleTask={toggleTask}
        />
    </div>
    
    </>
  )
}

export default App
