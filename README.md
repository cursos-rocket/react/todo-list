# ToDo List

Desafio 01 proposto na formação ReactJS promovida pela **Rocketseat**. Uma aplicação de controle de tarefas no estilo to-do list, que contém as seguintes funcionalidades:
- Adicionar uma nova tarefa
- Marcar e desmarcar uma tarefa como concluída
- Remover uma tarefa da listagem
- Mostrar o progresso de conclusão das tarefas

Acesse [aqui](https://efficient-sloth-d85.notion.site/Desafio-01-Praticando-os-conceitos-do-ReactJS-91fd63dd1a5b4a2796152de293ec1074) a descrição completa do desafio.

![Nível](https://img.shields.io/badge/nível-Básico-green?style=for-the-badge)

![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)

***

### Preview da Aplicação:
![Preview da aplicação](preview.gif)